# ![windward](https://bitbucket.org/monkygames/puppet-windward/downloads/logo.png) Windward Dedicated Server Puppet Module

## Overview
Deploys and manages a dedicated Windward Game Server.

## Requirements
The following are requirements for running the Windward Server.

* RAM: 1GB
* OS: Ubuntu Server 16.04 64-bit or higher
* Network: ports 5127 open (default)

## Classes


### Windward
The windward class manages the windward dedicated server.

#### `server_name`
The name of the server.

#### `world`
The world file to load (or the world name if starting from scatch).  
Default: 'world'

#### `port`
The tcp port to use.  
Default: 5127

#### `is_public`
If true, registers the dedicated server to the public lobby.  
Default: false

#### `admin_list`
An array of Steam IDs. The first id should be the owner's.
To find your steam id, you can search [here](https://steamid.io/lookup)
Or while in Windward game, type '/info'.  IDs are 17 numbers long.  
Default: undef

## Limitations

Works with Debian Based (systemd) OS's only.
Currently, using a windward as a service doesn't work.  The reason is that
the Server requires console input ('q' to properly shutdown the server).
I will contact the dev and ask if there is a way to start the server in 
non interactive mode.