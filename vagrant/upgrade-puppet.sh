#!/bin/bash

DISTRIB_CODENAME=$(lsb_release --codename --short)
DEB="puppetlabs-release-pc1-${DISTRIB_CODENAME}.deb"
DEB_PROVIDES="/etc/apt/sources.list.d/puppetlabs-pc1.list" 

if [ ! -e $DEB_PROVIDES ]
then
    apt-get install --yes lsb-release
    wget -q http://apt.puppetlabs.com/$DEB
    dpkg -i $DEB

    apt-get update
    apt-get install byobu
    apt-get install vim
    apt-get install --yes puppet-agent
    gem install hiera-eyaml
    echo '_byobu_sourced=1 . /usr/bin/byobu-launch' >> /home/vagrant/.profile
fi
