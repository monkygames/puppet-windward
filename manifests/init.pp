# == Class: windward
#
# Installs the windward server.
#
# See: http://windward.gamepedia.com/Dedicated_Server
#
# === Parameters
#
# [*server_name*]
#   The name of the server.
#
# [*world*]
#   The world file to load (or the world name if starting from scatch)
#   Default: 'world'
#
# [*port*]
#   The tcp port to use.
#   Default: 5127
#
# [*is_public*]
#   If true, registers the dedicated server to the public lobby
#   Default: false
#
# [*admin_list*]
#   An array of Steam IDs. The first id should be the owner's.
#   To find your steam id, you can search here:
#   https://steamid.io/lookup/
#   Or while in Windward game, type '/info'.  IDs are 17 numbers long.
#  Default: undef
#
# === Variables
#
# [*root_dir*]
#   The root directory to install the dedicated server.
#
# [*service*]
#   The file name and path that runs the server.
#
# [*systemd*]
#   The systemd file name and path.
#
# [*pidfile*]
#   The path and file name of the pid.
#
# [*conf*]
#  The path and file name of the configuration file.
#
# [*runfile*]
#   The full path to the init file that.
#
# === Copyright
#
# GPL-v3.0+
#
class windward (
  String  $server_name,
  String  $world     = 'world',
  Integer $port      = 5127,
  Boolean $is_public = false,
  $admin_list        = undef,
){
  anchor{'windward::begin':}

  # variables
  $root_dir    = '/opt/windward'
  $systemd     = '/etc/systemd/system/windward.service'
  $pidfile     = "${root_dir}/server.pid"
  $conf        = "${root_dir}/server.conf"
  $admin_dir   = "${root_dir}/ServerConfig"
  $admin_file  = "${admin_dir}/admin.txt"
  $lib         = "${root_dir}/lib"
  $cmd         = "${lib}/WWServer.exe"
  $servicefile = "${lib}/windward_service.bash"
  $runfile     = "${root_dir}/windward.bash"

  # == install ==
  package{'mono-complete':
    ensure  => installed,
    require => Anchor['windward::begin'],
  }

  # == configuration ==
  # create a user
  user{'windward':
    managehome => false,
    home       => $root_dir,
    require    => Package['mono-complete'],
  }

  file{$root_dir:
    ensure  => directory,
    owner   => 'windward',
    group   => 'windward',
    require => User['windward'],
  }

  file{$conf:
    ensure  => file,
    owner   => 'windward',
    group   => 'windward',
    content => template('windward/server.conf.erb'),
    require => File[$root_dir],
  }

  file{$lib:
    ensure  => directory,
    owner   => 'windward',
    group   => 'windward',
    require => File[$conf],
  }

  file{$cmd:
    ensure  => file,
    mode    => '0755',
    owner   => 'windward',
    group   => 'windward',
    source  => 'puppet:///modules/windward/WWServer.exe',
    require => File[$lib],
  }

  file{$servicefile:
    ensure  => file,
    mode    => '0755',
    owner   => 'windward',
    group   => 'windward',
    content => template('windward/windward_service.bash.erb'),
    require => File[$cmd],
  }

  file{$admin_dir:
    ensure  => directory,
    owner   => 'windward',
    group   => 'windward',
    require => File[$servicefile],
  }

  file{$admin_file:
    ensure  => file,
    owner   => 'windward',
    group   => 'windward',
    content => epp('windward/admin.txt.epp',{'admin_list' => $admin_list}),
    require => File[$admin_dir],
  }

  file{$runfile:
    ensure  => file,
    mode    => '0755',
    owner   => 'windward',
    group   => 'windward',
    content => epp('windward/windward.bash.epp',
    {'servicefile' => $servicefile}),
    require => File[$admin_file],
  }

  file{$systemd:
    ensure  => file,
    mode    => '0755',
    content => template('windward/windward.service.erb'),
    require => File[$runfile],
  }

  # reload
  exec { 'systemctl-daemon-reload':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
    subscribe   => File[$systemd],
  }


  # == service ==
#  service{'windward':
#    ensure    => running,
#    subscribe => File[$systemd,$conf,$runfile,$service],
#    require   => Exec['systemctl-daemon-reload'],
#  }

  anchor{'windward::end':
    #require => Service['windward'],
    require  => Exec['systemctl-daemon-reload'],
  }
}